## OpenMP_MPI Guide ##

This repository contains self-made notes, links to available tutorials and blogs, and codes that cover topics related to vectorization (OpenMP SIMD), multithreading (OpenMP), and cluster-computing (MPI) using Intel, GNU-GCC and LLVM-CLANG compilers.

The topics included in this guide are OpenMP, SIMD, MPI, SSE / AVX instructions, mutexes, data and cache locality (loop fusion, tiling and mining), high-bandwidth memory (HBM), NUMA, strides, compiler-optimization, and qsub.


### Platform Configuration ###

* Fedora 32
* Intel i7-3770, 16GB DDR3
* [Intel System Studio 2020](https://software.intel.com/en-us/system-studio) v19.1.2.254
* [Intel OneAPI](https://software.intel.com/content/www/us/en/develop/tools/oneapi.html) - Base + HPC Toolkit v2021.1
* GNU-GCC v10.1.1
* CLANG v10.0.0
* OpenMP v4.5

### Setup ###

* OpenMP, MPI, Boost MPI installation (for clang and gcc) -- OpenMPI, MPICH
```
dnf install libomp.x86_64 libomp-devel.x86_64 libgomp.x86_64 openmpi.x86_64 openmpi-devel.x86_64 mpich.x86_64 boost-openmpi.x86_64 boost-openmpi-devel.x86_64
```
```
# Configure OpenMP
sudo vim /etc/profile.d/openmpi.sh
```
```
export OPENMPI_HOME=/usr/lib64/openmpi
export PATH=$PATH:${OPENMPI_HOME}/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${OPENMPI_HOME}/lib
```
* Intel System Studio installation
* To use Intel's compiler (System Studio) in CLI mode
```
sudo vim /etc/profile.d/intel_system_studio.sh
```
```
export INTEL_SDK=$HOME/intel/system_studio_2020
export PATH=$PATH:${INTEL_SDK}/bin:${INTEL_SDK}/opencl/SDK/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${INTEL_SDK}/lib:${INTEL_SDK}/lib/intel64:${INTEL_SDK}/opencl/SDK/lib64
export INCLUDE_DIRS=$INCLUDE_DIRS:${INTEL_SDK}/include:${INTEL_SDK}/opencl/SDK/include
```
* Load Intel's compiler using bash aliases.
* Create bash aliases to easily load Intel's compiler and other aliases that you may have created. - "vim $HOME/.bash_aliases"
```
# PYTHONPATH
export PYTHONPATH="${PYTHONPATH}:/usr/lib64/python3.8/site-packages/openmpi"

# Alias for Python 3.8 virtual environment
alias py3.8venv="source $HOME/.virtualenvs/py3.8venv/bin/activate"

# Alias for Intel OneAPI
alias inteloneapi="source $HOME/intel/inteloneapi/setvars.sh"

# Alias for Intel System Studio
alias intelsystemstudio="source $HOME/intel/system_studio_2020/compilers_and_libraries/linux/bin/compilervars.sh -arch intel64 -platform linux"
```
* Edit .bashrc and then "source $HOME/.bashrc"
```
if [ -e $HOME/.bash_aliases ]; then
  source $HOME/.bash_aliases
fi
```


### Sources ###

* [Lawrence Livermore National Laboratory - HPC Tutorials](https://hpc.llnl.gov/training/tutorials)
* [Coursera - Fundamentals of Parallelism on Intel Architecture](https://www.coursera.org/learn/parallelism-ia)
* [Intel Vectorization Guide](https://software.intel.com/en-us/articles/vectorization-essential)
* [Intel Compiler Options for Intel SSE and Intel AVX Generation](https://software.intel.com/en-us/articles/performance-tools-for-software-developers-intel-compiler-options-for-sse-generation-and-processor-specific-optimizations)
* [Colfax Research Training](https://colfaxresearch.com/training/)
* [Intel Xeon Phi Optimization Blog - Multithreading and Parallelism](http://www.techenablement.com/intel-xeon-phi-optimization-part-1-of-3-multi-threading-and-parallel-reduction/)
* https://easyperf.net/blog/2017/10/24/Vectorization_part1
* [Strided memory access on CPUs, GPUs, and MIC](https://www.karlrupp.net/2016/02/strided-memory-access-on-cpus-gpus-and-mic/)
* [Optimization techniques for the Intel MIC architecture](https://software.intel.com/en-us/articles/optimization-techniques-for-the-intel-mic-architecture-part-1-of-3)
* [Explicit vector programming with OpenMP](http://www.hpctoday.com/hpc-labs/explicit-vector-programming-with-openmp-4-0-simd-extensions/)
* [OpenMP in a nutshell](https://www.bowdoin.edu/~ltoma/teaching/cs3225-GIS/fall17/Lectures/openmp.html)
* [Data Alignment to Assist Vectorization](https://software.intel.com/en-us/articles/data-alignment-to-assist-vectorization)
* [GCC Auto-Vectorization](https://gcc.gnu.org/projects/tree-ssa/vectorization.html)
* [GCC Vectorization reports](https://gcc.gnu.org/ml/gcc-patches/2005-01/msg01247.html)
* [Fast Numerical Programming with GCC using SIMD](https://berthub.eu/gcc-simd/index.html)
* [Open MPI](https://www.open-mpi.org)
* [MPICH](https://www.mpich.org/)
* [Intel MPI](https://software.intel.com/content/www/us/en/develop/tools/mpi-library.html)
