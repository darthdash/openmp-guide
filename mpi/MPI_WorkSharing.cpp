/* mpiicpc -qopenmp -Wall -Wextra WorkSharing.cpp */
/* mpiCC -qopenmp -Wall -Wextra WorkSharing.cpp */
/* mpirun ./a.out */
/* mpirun -n 8 ./a.out */

#include <iostream>
#include <cmath>
#include <omp.h>
#include <cstdio>
#include <vector>

void parallel_for()
{
  const int length{1024 * 1024 * 64};
  float *a = new float[length];
  float *b = new float[length];
  float *c = new float[length];
  float *result = new float[length];

#pragma omp parallel for
  for (size_t i = 0; i < length; i++)
  {
    result[i] = a[i] + b[i] * std::erff(c[i]);
  }

  delete[] a;
  delete[] b;
  delete[] c;
  delete[] result;
}

void sections()
{
#pragma omp parallel sections
  {
#pragma omp section
    {
      for (size_t i = 0; i < 1000; i++)
      {
        std::cout << i;
      }
    }

#pragma omp section
    {
      for (size_t i = 0; i < 1000; i++)
      {
        // Cycle all characters indefinitely.
        std::cout << static_cast<char>('a' + (i % 26));
      }
    }
  }
}

void single_master()
{
#pragma omp parallel
  {
#pragma omp single
    {
      std::cout << "Gathering input: " << omp_get_thread_num() << "\n";
      std::cout << "parallel on: " << omp_get_thread_num() << "\n";
    }

#pragma omp barrier

#pragma omp master
    {
      std::cout << "output on: " << omp_get_thread_num() << "\n";
    }
  }
}

void atomic()
{
  std::cout << "ATOMIC"
            << "\n";
  int sum{0};

#pragma omp parallel for num_threads(128)
  for (size_t i = 0; i < 100; i++)
  {
#pragma omp atomic
    ++sum;
  }

  std::cout << sum << "\n";
}

void ordered()
{
  std::cout << "ORDERED"
            << "\n";
  std::vector<int> squares;

#pragma omp parallel for ordered
  for (size_t i = 0; i < 20; i++)
  {
    std::cout << "Current thread: " << omp_get_thread_num() << i << "\n";
    int j = i * i;

#pragma omp ordered
    squares.push_back(j);
  }
  std::cout << "\n";
  for (auto v : squares)
  {
    std::cout << v << "\t";
  }
  std::cout << "\n";
}

void data_sharing()
{
  int i{10};

// #pragma omp parallel for private(i)
// #pragma omp parallel for firstprivate(i)
#pragma omp parallel for lastprivate(i)
  for (int a = 0; a < 10; a++)
  {
    printf("Thread %d, i = %d\n", omp_get_thread_num(), i);
    i = 1000 + omp_get_thread_num();
  }

  std::cout << i << "\n";
}

int main(int argc, char const *argv[])
{
  // parallel_for();
  // sections();
  // single_master();
  // atomic();
  // ordered();
  data_sharing();
  return 0;
}
