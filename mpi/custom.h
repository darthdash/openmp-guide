#pragma once

#include <iostream>
#include <string>
#include <iomanip>
#include <math.h>
#include <random>
#include <mpi.h>
#include <thread>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>