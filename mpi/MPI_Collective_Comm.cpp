/* Collective Communication using MPI */

#include "custom.h"

int broadcast(int argc, char *argv[])
{
  int size, rank;

  MPI_Init(&argc, &argv);

  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int value{42};
  MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD); /* Root has rank 0 */

  std::cout << "Rank: " << rank << " received from 0, the value is: " << value << "\n";

  // Pause threads until all finish executing
  MPI_Barrier(MPI_COMM_WORLD);

  std::cout << "Rank " << rank << " finished working."
            << "\n";

  MPI_Finalize();
  return 0;
}

int reduce(int argc, char *argv[])
{
  int size, rank;

  MPI_Init(&argc, &argv);

  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  const int itemsPerProcess{10};
  const int count{size * itemsPerProcess};
  int *data = new int[count];

  if (rank == 0)
  {
    for (size_t i = 0; i < count; i++)
    {
      data[i] = rand() % 10;
    }
  }

  // Scatter operation
  int *localData = new int[itemsPerProcess]; /* Placeholder to collect the data */
  MPI_Scatter(data, itemsPerProcess, MPI_INT, localData, itemsPerProcess, MPI_INT, 0, MPI_COMM_WORLD);

  int localSum{0};
  for (size_t i = 0; i < itemsPerProcess; i++)
  {
    localSum += localData[i];
  }

  // Reduce operation
  int globalSum{};
  MPI_Reduce(&localSum, &globalSum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); /* Count is 1 element per process */

  if (rank == 0)
  {
    std::cout << "Total sum using MPI reduction = " << globalSum << "\n";
  }

  delete[] localData;
  delete[] data;

  MPI_Finalize();

  return 0;
}

int main(int argc, char *argv[])
{
  std::random_device rd{};
  srand(rd());

  return broadcast(argc, argv);

  // return reduce(argc, argv);
}
