#include "custom.h"

int main(int argc, char *argv[])
{
  MPI_Init(&argc, &argv);

  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0)
  {
    int n;
    std::cout << "Enter a number: ";
    std::cin >> n;
    for (int i = 1; i < size; i++)
    {
      MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
      std::cout << "\n";
    }
  }
  else
  {
    int n;
    MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    std::cout << n << "^" << rank << " = " << std::pow(n, rank);
  }

  std::cout << "\n";

  MPI_Finalize();
  return 0;
}
