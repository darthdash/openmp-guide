#include <cmath>

double BlackBoxFunction(double x) {
	return 1.0/sqrt(x);
}

double InverseDerivative(double x) {
	return 2.0/sqrt(x);
}